﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Falling_Blocks.Models
{
    class Blocks
    {
        public const int size = 20;
        private Random rnd = new Random();
        private List<SolidColorBrush> blockColorsList = new List<SolidColorBrush>()
        {
            Brushes.Green,
            Brushes.DarkGreen,
            Brushes.LightGreen,
            Brushes.Lime,
            Brushes.Blue,
            Brushes.LightBlue,
            Brushes.LightSeaGreen,
            Brushes.Cyan,
            Brushes.LightSkyBlue,
            Brushes.Indigo,
            Brushes.Purple,
            Brushes.Fuchsia,
            Brushes.DeepPink,
            Brushes.Violet,
            Brushes.BlueViolet,
            Brushes.DarkViolet
        };

        public UIElement UiElement { get; set; }

        public Point Position { get; set; }

        public SolidColorBrush color { get; set; }

        public Blocks()
        {
            color = GetRandomColor();
        }

        private SolidColorBrush GetRandomColor ()
        {
            var x = rnd.Next(blockColorsList.Count);

            return blockColorsList[x];
        }
    }
}
