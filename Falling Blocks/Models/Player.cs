﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Falling_Blocks.Models
{

    class Player
    {
        public Ellipse playerModel = new Ellipse()
        {
            Width = 35,
            Height = 35,
            Stroke = Brushes.DarkOrange,
            StrokeThickness = 1,
            Fill = Brushes.Gray,
            HorizontalAlignment = HorizontalAlignment.Center,
            VerticalAlignment = VerticalAlignment.Bottom
        };

        public Point Position { get; set; }

    }
}
