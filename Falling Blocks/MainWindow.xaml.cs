﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Falling_Blocks.Models;

namespace Falling_Blocks
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region private variables
        private Random rnd = new Random();
        private DispatcherTimer gameTimer = new DispatcherTimer();
        private int tickCounter = 0;
        const int StartSpeed = 20;
        const int SpeedThreshold = 100;
        private List<Blocks> allBlocks = new List<Blocks>();
        private Player player = new Player();

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            gameTimer.Tick += GameTimer_Tick; 
        }



        #region Events

        private void StartGame()
        {
            player.Position = new Point(((GameArea.ActualWidth/2)-player.playerModel.ActualWidth), GameArea.ActualHeight - player.playerModel.Height);
            GenerateBlock();
            DrawBlocks();

            gameTimer.Interval = TimeSpan.FromMilliseconds(StartSpeed);
            gameTimer.IsEnabled = true;
        }
        private void GameTimer_Tick(object sender, EventArgs e)
        {
            tickCounter++;
            ClearGameArea();
            MoveBlocks();
            DrawPlayer();
            DrawBlocks();
            if (tickCounter == 15)
            {
                GenerateBlock();
                tickCounter = 0;
            }
        }
        private void MainWindow_OnContentRendered(object sender, EventArgs e)
        {
            DrawGameArea();
            StartGame();
        }
        #endregion

        #region DrawMethods
        private void DrawGameArea()
        {
            bool doneDrawing = false;
            int nextX = 0;
            int nextY = 0;
            int rowCounter = 0;
            int cellSize = Blocks.size;

            while (!doneDrawing)
            {
                Rectangle rect = new Rectangle()
                {
                    Height = cellSize,
                    Width = cellSize
                };
                GameArea.Children.Add(rect);
                Canvas.SetTop(rect, nextY);
                Canvas.SetLeft(rect, nextX);

                nextX += cellSize;
                if (nextX >= GameArea.ActualWidth)
                {
                    nextX = 0;
                    nextY += cellSize;
                    rowCounter++;
                }

                if (nextY >= GameArea.ActualHeight) doneDrawing = true;
            }
        }
        private void DrawPlayer()
        {
            GameArea.Children.Add(player.playerModel);
            Canvas.SetTop(player.playerModel, player.Position.Y);
            Canvas.SetLeft(player.playerModel, player.Position.X);
        }
        private void DrawBlocks()
        {
            foreach (Blocks block in allBlocks)
            {
                GameArea.Children.Add(block.UiElement);
                Canvas.SetTop(block.UiElement, block.Position.Y);
                Canvas.SetLeft(block.UiElement, block.Position.X);
            }
        }
        private void ClearGameArea()
        {
            foreach (Blocks block in allBlocks)
            {
                if (block.UiElement != null)
                {
                    GameArea.Children.Remove(block.UiElement);
                }
            }
            GameArea.Children.Remove(player.playerModel);
        }
        private void GenerateBlock()
        {
            var block = new Blocks
            {
                Position = new Point(rnd.Next(Blocks.size, ((int) GameArea.ActualWidth - Blocks.size)), Blocks.size)
            };
            block.UiElement = new Rectangle()
            {
                Width = Blocks.size,
                Height = Blocks.size,
                Fill = block.color
            };
            allBlocks.Add(block);
        }
        private void MoveBlocks()
        {
            foreach (Blocks block in allBlocks)
            {
                block.Position = new Point(block.Position.X, block.Position.Y+(Blocks.size/8));
            }
        }
        #endregion

        private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Right)
            {
                player.Position = new Point(player.Position.X + (player.playerModel.ActualWidth/ 6), player.Position.Y);
            }
            if (e.Key == Key.Left) player.Position = new Point(player.Position.X - (player.playerModel.ActualWidth / 6), player.Position.Y);
        }
    }
}
